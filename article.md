title: "Kauza Jiřího Kajínka událost za událostí"
perex: "Případ nejznámějšího vězně přehledně"
authors: ["Lukáš Řezník"]
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/kajinek_170512-141008_cib.jpg
coverimg_note: "Foto ČTK"
styles: []
libraries: []
---

Prezident Miloš Zeman podepíše rozhodnutí o udělení milosti doživotně odsouzenému Jiřímu Kajínkovi. Připravili jsme pro vás časovou osu s událostmi v kauze Jiřího Kajínka.

Plzeňský soud uložil zřejmě nejznámějšímu českému vězni doživotní trest 23. června 1998. Odsoudil ho za to, že v roce 1993 zastřelil v serpentinách pod plzeňskou věznicí Bory podnikatele Štefana Jandu a jeho osobního strážce Juliána Pokoše. Druhý bodyguard útok přežil.

Na dodržování sedmileté podmínky, která má být součástí milosti prezidenta Miloše Zemana pro doživotně odsouzeného dvojnásobného vraha Jiřího Kajínka, by dohlížel Krajský soud v Plzni. Vyplývá to z trestního řádu. Miloš Zeman zastával v minulosti názor, že by odsouzeného Kajínka nepropouštěl. V dubnu tohoto roku ale změnil názor.

<iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1A3rdhAR7pMaRdQEJU3JGjJduXPV6cztHNUjNTLKWPAE&font=OpenSans-GentiumBook&lang=cz&initial_zoom=2&height=650' width='100%' max-width="1000" height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>